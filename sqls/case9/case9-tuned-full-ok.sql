--0dj4arrwaakk5
var a0  VARCHAR2(32)  ;
var a1  VARCHAR2(32)  ;
var a2  VARCHAR2(32)  ;
var a3  VARCHAR2(128) ;
var a4  VARCHAR2(128) ;
var a5  VARCHAR2(128) ;
var a6  VARCHAR2(128) ;
var a7  VARCHAR2(128) ;
var a8  VARCHAR2(128) ;
var a9  VARCHAR2(128) ;
var a10 VARCHAR2(128) ;
var a11 VARCHAR2(128) ;
var a12 VARCHAR2(32)  ;
var a13 VARCHAR2(32)  ;
var a14 VARCHAR2(32)  ;
var a15 VARCHAR2(32)  ;
var a16 VARCHAR2(32)  ;
var a17 VARCHAR2(32)  ;
var a18 VARCHAR2(32)  ;
var a19 VARCHAR2(32)  ;
var a20 VARCHAR2(32)  ;
var a21 VARCHAR2(32)  ;
var a22 VARCHAR2(32)  ;
var a23 VARCHAR2(32)  ;
var a24 VARCHAR2(32)  ;
var a25 NUMBER    ;
var a26 VARCHAR2(2000);
var a27 VARCHAR2(128) ;
var a28 VARCHAR2(32)  ;
var a29 VARCHAR2(32)  ;

exec :a0  := '100'       ;
exec :a1  := '100'       ;
exec :a2  := '100'       ;
exec :a3  := 'S000000039';
exec :a4  := 'S000000080';
exec :a5  := 'S000000199';
exec :a6  := 'S000000200';
exec :a7  := 'S000000308';
exec :a8  := 'S000000519';
exec :a9  := 'S000000515';
exec :a10 := 'S000004985';
exec :a11 := 'S000004986';
exec :a12 := 'D11'  ;
exec :a13 := '01'        ;
exec :a14 := '20140317'  ;
exec :a15 := '20140318'  ;
exec :a16 := 'D22'       ;
exec :a17 := '02'        ;
exec :a18 := '03'        ;
exec :a19 := '04'        ;
exec :a20 := '20140317'  ;
exec :a21 := '20140318'  ;
exec :a22 := 'D31'       ;
exec :a23 := '20140317'  ;
exec :a24 := '20140318'  ;
exec :a25 := 0           ;
exec :a26 := ' '         ;
exec :a27 := ' '         ;
exec :a28 := '100'       ;
exec :a29 := 'X'         ;
                         
SELECT  /*+leading(t_00) full(t_00) use_nl(t_01 t_00 t_02) */ T_00."MANDT",
       T_00."ZVBELN",
       T_00."ZPOSNR",
       T_00."FKDAT",
       T_00."LIFNR",
       T_00."NODEKEY",
       T_00."ERDAT",
       T_00."ERZET",
       T_00."ERNAM",
       T_00."AEDAT",
       T_00."AEZET",
       T_00."AENAM",
       T_00."ZCRE_INTID",
       T_00."ZCHG_INTID",
       T_01."MANDT",
       T_01."ZVBELN",
       T_01."ZPOSNR",
       T_01."VBELN",
       T_01."POSNR",
       T_01."ETENR",
       T_01."BSTNK",
       T_01."BSTNK2",
       T_01."OPUID",
       T_01."MEMID",
       T_01."REALREQUESTOR",
       T_01."DEPARTID",
       T_01."DELIVERYADDRID",
       T_01."WE_NAME1",
       T_01."MDID",
       T_01."CSID",
       T_01."BUDGETDEPARTID",
       T_01."ACCOUNTID",
       T_01."SUBDEPARTID",
       T_01."CHARG",
       T_01."MATNR",
       T_01."STOCKNO",
       T_01."NPKTX",
       T_01."NAMECARDID",
       T_01."ORG_KWMENG",
       T_01."KWMENG",
       T_01."VRKME",
       T_01."NETPR",
       T_01."WAERK",
       T_01."KPEIN",
       T_01."KMEIN",
       T_01."TMWSTS",
       T_01."ZNETPR",
       T_01."SUKURS",
       T_01."FFACT",
       T_01."TFACT",
       T_01."RECAMT",
       T_01."ZRSPEC",
       T_01."MFRNR",
       T_01."TAXM1",
       T_01."PMCODE",
       T_01."UCQTY",
       T_01."PMCOST",
       T_01."PMNETPR",
       T_01."DCAMT",
       T_01."DCRATE",
       T_01."LIFNR",
       T_01."BPREI",
       T_01."WAERS",
       T_01."PEINH",
       T_01."BPRME",
       T_01."DEVKURKAUF",
       T_01."DFFACT",
       T_01."DTFACT",
       T_01."MWSKP",
       T_01."QTYDCF",
       T_01."ORGINCOMEPRICE",
       T_01."ORGPRICE",
       T_01."OPTFX",
       T_01."DEVIDECD",
       T_01."KOSTL",
       T_01."ANLN1",
       T_01."POSID",
       T_01."REFITEM_D",
       T_01."REFITEM_R",
       T_01."KZTLF_R",
       T_01."KZTLF_P",
       T_01."WERKS",
       T_01."EMWRK",
       T_01."TRANSTYPE",
       T_01."KONSI",
       T_01."VSART",
       T_01."VSBED",
       T_01."LIDAT",
       T_01."TP_KUNNR",
       T_01."FRLIF",
       T_01."BF_INVNOEB",
       T_01."ROUTE",
       T_01."CALCDATF",
       T_01."STATUS",
       T_01."ABGRU",
       T_01."CALCSTATUS",
       T_01."CALCSTATUS2",
       T_01."DLVRATE",
       T_01."EBBEX",
       T_01."GRBEX",
       T_01."CALCDAT",
       T_01."AUTOORDERF",
       T_01."AUTOORDERDAT",
       T_01."PREDLV",
       T_01."POSTCODE",
       T_01."STREET",
       T_01."CITY1",
       T_01."TEL_NUMBER",
       T_01."MOB_NUMBER",
       T_01."SHIPLINEID",
       T_01."EBELN",
       T_01."EBELP",
       T_01."MJAHR",
       T_01."MBLNR",
       T_01."ZEILE",
       T_01."GRMJAHR",
       T_01."GRMBLNR",
       T_01."GRZEILE",
       T_01."NPRNO",
       T_01."ZTERM",
       T_01."CUSTCODEID1",
       T_01."CUSTCODEID2",
       T_01."CUSTCODEID3",
       T_01."CUSTCODEID4",
       T_01."CUSTCODEID5",
       T_01."OBJ_KEY",
       T_01."OBJ_KEY1",
       T_01."OBJ_KEY2",
       T_01."BOSNO",
       T_01."ZORUN",
       T_01."RECAMT_W",
       T_01."CENTER_CD",
       T_01."ACCT_CD",
       T_01."DEPT_CD",
       T_01."COST_DEPT_C",
       T_01."MPC_FLAG",
       T_01."EXPTP",
       T_01."AMKNZ",
       T_01."NODEKEY",
       T_01."RKWMENG",
       T_01."INBOUND",
       T_01."TAX_CD",
       T_01."VKGRP",
       T_01."VKBUR",
       T_01."EXCHG_YN",
       T_01."BEXGUBN",
       T_01."BEXCODE",
       T_01."CUSTORDGB",
       T_01."INVEST_CD",
       T_01."GIMJAHR",
       T_01."GIMBLNR",
       T_01."GIZEILE",
       T_01."CONSN_RT",
       T_01."SCIID",
       T_01."SCIDEPTCD",
       T_01."SCIF",
       T_01."SCIPRDHA",
       T_01."SCIPRDHA_T",
       T_01."SCIPRDHA_M",
       T_01."SCISYSID",
       T_01."IVDAT",
       T_01."FKDAT",
       T_01."LGEGBMSSEQNO",
       T_01."LGEGBMSSEQF",
       T_01."LGEGBMSDAT",
       T_01."PACKINGNO",
       T_01."ERDAT",
       T_01."ERZET",
       T_01."ERNAM",
       T_01."AEDAT",
       T_01."AEZET",
       T_01."AENAM",
       T_01."ZCRE_INTID",
       T_01."ZCHG_INTID",
       T_02."MANDT",
       T_02."ZVBELN",
       T_02."VKGRP",
       T_02."VKBUR",
       T_02."VBELN",
       T_02."NODEKEY",
       T_02."KUNNR",
       T_02."WAERK",
       T_02."ERDAT",
       T_02."ERZET",
       T_02."ERNAM",
       T_02."AEDAT",
       T_02."AEZET",
       T_02."AENAM",
       T_02."ZCRE_INTID",
       T_02."ZCHG_INTID"
FROM sapsr3."ZSDT202" T_00
INNER JOIN sapsr3."ZSDT201" T_01 
  ON T_01."MANDT"=:A0
    AND T_00."ZVBELN"=T_01."ZVBELN"
    AND T_00."ZPOSNR"=T_01."ZPOSNR"
INNER JOIN sapsr3."ZSDT200" T_02 
  ON T_02."MANDT"=:A1
  AND T_01."ZVBELN"=T_02."ZVBELN"
WHERE T_00."MANDT"=:A2
  AND T_02."NODEKEY" IN (:A3,:A4,:A5,:A6,:A7,:A8,:A9,:A10,:A11)  
  AND (
           T_01."STATUS"=:A12
       AND T_01."TRANSTYPE"=:A13
       AND T_00."GIDDAT" BETWEEN :A14 AND :A15
       OR T_01."STATUS"=:A16
       AND T_01."TRANSTYPE" IN (:A17,:A18,:A19)
       AND T_00."HUBGIDAT" BETWEEN :A20 AND :A21
       OR T_01."STATUS"=:A22
       AND T_00."ENDDEVDAT" BETWEEN :A23 AND :A24
       )
  AND T_01."KWMENG" >:A25
  AND T_01."BSTNK2" <>:A26
  AND T_01."GRBEX" <>:A27
  AND NOT EXISTS
  (SELECT T_100."MANDT"
     FROM sapsr3."ZMMT702" T_100
     WHERE T_100."MANDT"=:A28
       AND T_100."ZVBELN"=T_02."ZVBELN"
       AND T_100."ZPOSNR"=T_01."ZPOSNR"
       AND T_100."KUNNR"=T_02."KUNNR"
       AND T_100."INFF"=:A29
    )