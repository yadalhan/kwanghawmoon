-- recommend createing index on ZFIT038(APPRONO)
-- num_distinct of approno : 73306
--0cqup7cyvp1nk
var b0 varchar2(32)
var b1 varchar2(32)
var b2 varchar2(32)
var b3 varchar2(32)
var b4 varchar2(32)
var b5 varchar2(32)
var b6 varchar2(32)

exec :b0 := '100'                  ;
exec :b1 := '1000'                 ;
exec :b2 := '201403170072IV0095535';
exec :b3 := '3'                    ;
exec :b4 := 'C'                    ;
exec :b5 := 'E'                    ;
exec :b6 := 'P'                    ;

SELECT MAX(EDOCID)
FROM sapsr3.ZFIT038
WHERE MANDT=:b0
AND BUKRS=:b1
AND APPRONO=:b2
AND NOT ZZSTAT IN (:b3,:b4,:b5,:b6)
;


