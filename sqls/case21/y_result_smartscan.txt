
Connected.

Session altered.

Elapsed: 00:00:00.00

Session altered.

Elapsed: 00:00:00.00

PL/SQL procedure successfully completed.

Elapsed: 00:00:00.01

PL/SQL procedure successfully completed.

Elapsed: 00:00:00.00

PL/SQL procedure successfully completed.

Elapsed: 00:00:00.00

PL/SQL procedure successfully completed.

Elapsed: 00:00:00.00

PL/SQL procedure successfully completed.

Elapsed: 00:00:00.00

PL/SQL procedure successfully completed.

Elapsed: 00:00:00.00

PL/SQL procedure successfully completed.

Elapsed: 00:00:00.00
0001293422

1 row selected.

Elapsed: 00:00:01.07
SQL_ID  5shwdjfynn32y, child number 1
-------------------------------------
SELECT MAX(EDOCID) FROM sapsr3.ZFIT038 WHERE MANDT=:b0 AND BUKRS=:b1
AND APPRONO=:b2 AND NOT ZZSTAT IN (:b3,:b4,:b5,:b6)

Plan hash value: 2148501211

--------------------------------------------------------------------------------------------------------------------------------
| Id  | Operation                  | Name    | Starts | E-Rows |E-Bytes| Cost (%CPU)| E-Time   | A-Rows |   A-Time   | Buffers |
--------------------------------------------------------------------------------------------------------------------------------
|   0 | SELECT STATEMENT           |         |      1 |        |       |  8014 (100)|          |      1 |00:00:01.07 |   29349 |
|   1 |  SORT AGGREGATE            |         |      1 |      1 |    39 |            |          |      1 |00:00:01.07 |   29349 |
|*  2 |   TABLE ACCESS STORAGE FULL| ZFIT038 |      1 |     14 |   546 |  8014   (1)| 00:01:37 |      1 |00:00:01.07 |   29349 |
--------------------------------------------------------------------------------------------------------------------------------

Query Block Name / Object Alias (identified by operation id):
-------------------------------------------------------------

   1 - SEL$1
   2 - SEL$1 / ZFIT038@SEL$1

Outline Data
-------------

  /*+
      BEGIN_OUTLINE_DATA
      IGNORE_OPTIM_EMBEDDED_HINTS
      OPTIMIZER_FEATURES_ENABLE('11.2.0.3')
      DB_VERSION('11.2.0.3')
      OPT_PARAM('_b_tree_bitmap_plans' 'false')
      OPT_PARAM('query_rewrite_enabled' 'false')
      OPT_PARAM('_index_join_enabled' 'false')
      OPT_PARAM('_optim_peek_user_binds' 'false')
      OPT_PARAM('_optimizer_use_feedback' 'false')
      OPT_PARAM('star_transformation_enabled' 'true')
      ALL_ROWS
      OUTLINE_LEAF(@"SEL$1")
      FULL(@"SEL$1" "ZFIT038"@"SEL$1")
      END_OUTLINE_DATA
  */

Predicate Information (identified by operation id):
---------------------------------------------------

   2 - storage(("APPRONO"=:B2 AND "ZZSTAT"<>:B3 AND "ZZSTAT"<>:B4 AND "ZZSTAT"<>:B5 AND "ZZSTAT"<>:B6 AND "MANDT"=:B0
              AND "BUKRS"=:B1))
       filter(("APPRONO"=:B2 AND "ZZSTAT"<>:B3 AND "ZZSTAT"<>:B4 AND "ZZSTAT"<>:B5 AND "ZZSTAT"<>:B6 AND "MANDT"=:B0
              AND "BUKRS"=:B1))

Column Projection Information (identified by operation id):
-----------------------------------------------------------

   1 - (#keys=0) MAX("EDOCID")[30]
   2 - "EDOCID"[VARCHAR2,30]


55 rows selected.

Elapsed: 00:00:00.05
cell physical IO bytes saved by storage index                             0
cell physical IO interconnect bytes returned by smart scan                0

2 rows selected.

Elapsed: 00:00:00.00