-- recommend creating index on ZSMTN31(MANDT,APPRID,APPRGB) to avoid full scan
-- 4xzkpxppq0j0t
var a1 varchar2(32)
var a2 varchar2(32)
var a3 varchar2(32)
var a4 varchar2(32)
exec :a1 := '100';
exec :a2 := '100';
exec :a3 := '100';
exec :a4 := 'M000533373';

SELECT
   NVL(SUM(COUNT(*)),0) AS APPROVAL  
FROM
   (  SELECT
      MANDT,
      KUNNR,
      NPDAT,
      NPSEQ,
      APPRSTEP,
      APPRID,
      MAX(STATUS) STATUS,
      MAX(APPRDAT) APPRDAT  
   FROM
      sapsr3.ZSMTN31  
   WHERE
      MANDT = :a1 
      AND APPRGB = 'A' 
   GROUP BY
      MANDT,
      KUNNR,
      NPDAT,
      NPSEQ,
      APPRSTEP,
      APPRID ) A 
LEFT OUTER JOIN
   (
      SELECT
         DISTINCT MANDT,
         NPDAT,
         NPSEQ,
         APPRSTEP+1 APPRSTEP  
      FROM
         sapsr3.ZSMTN31  
      WHERE
         MANDT = :a2 
         AND STATUS = '02' 
         AND APPRGB = 'A' 
   ) A2 
      ON (
         A.MANDT = A2.MANDT 
         AND A.NPDAT = A2.NPDAT 
         AND A.NPSEQ = A2.NPSEQ 
         AND A.APPRSTEP = A2.APPRSTEP
      ) 
INNER JOIN
   sapsr3.ZSMTN10 B 
      ON(
         A.MANDT = B.MANDT 
         AND A.KUNNR = B.KUNNR 
         AND A.NPDAT = B.NPDAT 
         AND A.NPSEQ = B.NPSEQ
      ) 
INNER JOIN
   sapsr3.ZSMT901 E 
      ON(
         A.MANDT = E.MANDT 
         AND CD_CLF = 'ZSTATUS06' 
         AND A.STATUS = E.CD
      )  
WHERE
   A.MANDT = :a3 
   AND A.APPRID = :a4 
   AND (
      A.APPRSTEP < 2 
      OR A.APPRDAT > 0 
      OR A2.MANDT>0
   ) 
   AND (
      A.APPRDAT = '00000000' 
      OR A.STATUS = '04'
   ) 
GROUP BY  A.NPDAT, A.NPSEQ
;