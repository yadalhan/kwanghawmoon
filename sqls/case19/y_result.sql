1 row selected.

Elapsed: 00:00:00.14
SQL_ID  a3swm25740y8g, child number 0
-------------------------------------
SELECT    NVL(SUM(COUNT(*)),0) AS APPROVAL FROM    (  SELECT
MANDT,       KUNNR,       NPDAT,       NPSEQ,       APPRSTEP,
APPRID,       MAX(STATUS) STATUS,       MAX(APPRDAT) APPRDAT    FROM
   sapsr3.ZSMTN31    WHERE       MANDT = :a1       AND APPRGB = 'A'
GROUP BY       MANDT,       KUNNR,       NPDAT,       NPSEQ,
APPRSTEP,       APPRID ) A LEFT OUTER JOIN    (       SELECT
DISTINCT MANDT,          NPDAT,          NPSEQ,          APPRSTEP+1
APPRSTEP       FROM          sapsr3.ZSMTN31       WHERE          MANDT
= :a2          AND STATUS = '02'          AND APPRGB = 'A'    ) A2
 ON (          A.MANDT = A2.MANDT          AND A.NPDAT = A2.NPDAT
   AND A.NPSEQ = A2.NPSEQ          AND A.APPRSTEP = A2.APPRSTEP       )
INNER JOIN    sapsr3.ZSMTN10 B       ON(          A.MANDT = B.MANDT
     AND A.KUNNR = B.KUNNR          AND A.NPDAT = B.NPDAT          AND
A.NPSEQ = B.NPSEQ       ) INNER JOIN    sapsr3.ZSMT901 E       ON(
    A.MANDT = E

Plan hash value: 2251531861

----------------------------------------------------------------------------------------------------------------------------------------------------------------------
| Id  | Operation                           | Name      | Starts | E-Rows |E-Bytes| Cost (%CPU)| E-Time   | A-Rows |   A-Time   | Buffers |  OMem |  1Mem | Used-Mem |
----------------------------------------------------------------------------------------------------------------------------------------------------------------------
|   0 | SELECT STATEMENT                    |           |      1 |        |       |  1844 (100)|          |      1 |00:00:00.13 |    5452 |       |       |          |
|   1 |  SORT AGGREGATE                     |           |      1 |      1 |   112 |  1844   (2)| 00:00:23 |      1 |00:00:00.13 |    5452 |       |       |          |
|   2 |   HASH GROUP BY                     |           |      1 |      1 |   112 |  1844   (2)| 00:00:23 |      0 |00:00:00.13 |    5452 |   751K|   751K|          |
|*  3 |    FILTER                           |           |      1 |        |       |            |          |      0 |00:00:00.13 |    5452 |       |       |          |
|   4 |     NESTED LOOPS OUTER              |           |      1 |      1 |   112 |  1843   (2)| 00:00:23 |      0 |00:00:00.13 |    5452 |       |       |          |
|   5 |      NESTED LOOPS                   |           |      1 |      1 |   100 |  1467   (1)| 00:00:18 |      0 |00:00:00.13 |    5452 |       |       |          |
|   6 |       NESTED LOOPS                  |           |      1 |      1 |    70 |  1466   (1)| 00:00:18 |      0 |00:00:00.13 |    5452 |       |       |          |
|   7 |        VIEW                         |           |      1 |      9 |   468 |  1466   (1)| 00:00:18 |      0 |00:00:00.13 |    5452 |       |       |          |
|*  8 |         FILTER                      |           |      1 |        |       |            |          |      0 |00:00:00.13 |    5452 |       |       |          |
|   9 |          HASH GROUP BY              |           |      1 |      9 |   522 |  1466   (1)| 00:00:18 |      0 |00:00:00.13 |    5452 |   792K|   792K|          |
|* 10 |           FILTER                    |           |      1 |        |       |            |          |      0 |00:00:00.13 |    5452 |       |       |          |
|* 11 |            TABLE ACCESS STORAGE FULL| ZSMTN31   |      1 |    435 | 25230 |  1465   (1)| 00:00:18 |      0 |00:00:00.13 |    5452 |       |       |          |
|* 12 |        INDEX UNIQUE SCAN            | ZSMT901~0 |      0 |      1 |    18 |     0   (0)|          |      0 |00:00:00.01 |       0 |       |       |          |
|* 13 |       INDEX UNIQUE SCAN             | ZSMTN10~0 |      0 |      1 |    30 |     1   (0)| 00:00:01 |      0 |00:00:00.01 |       0 |       |       |          |
|  14 |      VIEW PUSHED PREDICATE          |           |      0 |      1 |    12 |   376   (2)| 00:00:05 |      0 |00:00:00.01 |       0 |       |       |          |
|  15 |       SORT UNIQUE                   |           |      0 |      1 |    27 |   376   (2)| 00:00:05 |      0 |00:00:00.01 |       0 | 73728 | 73728 |          |
|* 16 |        FILTER                       |           |      0 |        |       |            |          |      0 |00:00:00.01 |       0 |       |       |          |
|* 17 |         TABLE ACCESS BY INDEX ROWID | ZSMTN31   |      0 |      1 |    27 |   375   (2)| 00:00:05 |      0 |00:00:00.01 |       0 |       |       |          |
|* 18 |          INDEX SKIP SCAN            | ZSMTN31~0 |      0 |      1 |       |   374   (2)| 00:00:05 |      0 |00:00:00.01 |       0 |       |       |          |
----------------------------------------------------------------------------------------------------------------------------------------------------------------------

Query Block Name / Object Alias (identified by operation id):
-------------------------------------------------------------

   1 - SEL$9D2EC191
   7 - SEL$4        / A@SEL$3
   8 - SEL$4
  11 - SEL$4        / ZSMTN31@SEL$4
  12 - SEL$9D2EC191 / E@SEL$6
  13 - SEL$9D2EC191 / B@SEL$5
  14 - SEL$639F1A6F / A2@SEL$1
  15 - SEL$639F1A6F
  17 - SEL$639F1A6F / ZSMTN31@SEL$2
  18 - SEL$639F1A6F / ZSMTN31@SEL$2

Outline Data
-------------

  /*+
      BEGIN_OUTLINE_DATA
      IGNORE_OPTIM_EMBEDDED_HINTS
      OPTIMIZER_FEATURES_ENABLE('11.2.0.3')
      DB_VERSION('11.2.0.3')
      OPT_PARAM('_b_tree_bitmap_plans' 'false')
      OPT_PARAM('query_rewrite_enabled' 'false')
      OPT_PARAM('_index_join_enabled' 'false')
      OPT_PARAM('_optim_peek_user_binds' 'false')
      OPT_PARAM('_optimizer_use_feedback' 'false')
      OPT_PARAM('star_transformation_enabled' 'true')
      ALL_ROWS
      OUTLINE_LEAF(@"SEL$4")
      OUTLINE_LEAF(@"SEL$639F1A6F")
      PUSH_PRED(@"SEL$9D2EC191" "A2"@"SEL$1" 12 11 10 9)
      OUTLINE_LEAF(@"SEL$9D2EC191")
      MERGE(@"SEL$55A76E8E")
      OUTLINE(@"SEL$2")
      OUTLINE(@"SEL$9D2EC191")
      MERGE(@"SEL$55A76E8E")
      OUTLINE(@"SEL$7")
      OUTLINE(@"SEL$55A76E8E")
      MERGE(@"SEL$6CD3A5F8")
      OUTLINE(@"SEL$6")
      OUTLINE(@"SEL$6CD3A5F8")
      MERGE(@"SEL$F1D6E378")
      OUTLINE(@"SEL$5")
      OUTLINE(@"SEL$F1D6E378")
      MERGE(@"SEL$1")
      OUTLINE(@"SEL$3")
      OUTLINE(@"SEL$1")
      NO_ACCESS(@"SEL$9D2EC191" "A"@"SEL$3")
      INDEX(@"SEL$9D2EC191" "E"@"SEL$6" ("ZSMT901"."MANDT" "ZSMT901"."CD_CLF" "ZSMT901"."CD"))
      INDEX(@"SEL$9D2EC191" "B"@"SEL$5" ("ZSMTN10"."MANDT" "ZSMTN10"."KUNNR" "ZSMTN10"."NPDAT" "ZSMTN10"."NPSEQ"))
      NO_ACCESS(@"SEL$9D2EC191" "A2"@"SEL$1")
      LEADING(@"SEL$9D2EC191" "A"@"SEL$3" "E"@"SEL$6" "B"@"SEL$5" "A2"@"SEL$1")
      USE_NL(@"SEL$9D2EC191" "E"@"SEL$6")
      USE_NL(@"SEL$9D2EC191" "B"@"SEL$5")
      USE_NL(@"SEL$9D2EC191" "A2"@"SEL$1")
      USE_HASH_AGGREGATION(@"SEL$9D2EC191")
      FULL(@"SEL$4" "ZSMTN31"@"SEL$4")
      USE_HASH_AGGREGATION(@"SEL$4")
      INDEX_SS(@"SEL$639F1A6F" "ZSMTN31"@"SEL$2" ("ZSMTN31"."MANDT" "ZSMTN31"."KUNNR" "ZSMTN31"."NPDAT" "ZSMTN31"."NPSEQ" "ZSMTN31"."NPITEMNO" "ZSMTN31"."APPRGB"
              "ZSMTN31"."GRPSEQ" "ZSMTN31"."APPRSTEP"))
      USE_HASH_AGGREGATION(@"SEL$639F1A6F")
      END_OUTLINE_DATA
  */

Predicate Information (identified by operation id):
---------------------------------------------------

   3 - filter((TO_NUMBER("A"."APPRSTEP")<2 OR TO_NUMBER("A"."APPRDAT")>0 OR TO_NUMBER("A2"."MANDT")>0))
   8 - filter((MAX("APPRDAT")='00000000' OR MAX("STATUS")='04'))
  10 - filter(:A1=:A3)
  11 - storage(("APPRID"=:A4 AND "APPRGB"='A' AND "MANDT"=:A1 AND "MANDT"=:A3))
       filter(("APPRID"=:A4 AND "APPRGB"='A' AND "MANDT"=:A1 AND "MANDT"=:A3))
  12 - access("E"."MANDT"=:A3 AND "CD_CLF"='ZSTATUS06' AND "A"."STATUS"="E"."CD")
  13 - access("B"."MANDT"=:A3 AND "A"."KUNNR"="B"."KUNNR" AND "A"."NPDAT"="B"."NPDAT" AND "A"."NPSEQ"="B"."NPSEQ")
  16 - filter((:A2=:A3 AND :A2="A"."MANDT"))
  17 - filter("STATUS"='02')
  18 - access("MANDT"=:A2 AND "NPDAT"="A"."NPDAT" AND "NPSEQ"="A"."NPSEQ" AND "APPRGB"='A')
       filter(("NPDAT"="A"."NPDAT" AND "NPSEQ"="A"."NPSEQ" AND "APPRGB"='A' AND TO_NUMBER("APPRSTEP")+1=TO_NUMBER("A"."APPRSTEP")))

Column Projection Information (identified by operation id):
-----------------------------------------------------------

   1 - (#keys=0) SUM(COUNT(*))[22]
   2 - "A"."NPDAT"[VARCHAR2,24], "A"."NPSEQ"[VARCHAR2,15], COUNT(*)[22]
   3 - "A"."NPDAT"[VARCHAR2,24], "A"."NPSEQ"[VARCHAR2,15]
   4 - "A"."NPDAT"[VARCHAR2,24], "A"."NPSEQ"[VARCHAR2,15], "A"."APPRSTEP"[VARCHAR2,6], "A"."APPRDAT"[VARCHAR2,24], "A2"."MANDT"[VARCHAR2,9]
   5 - "A"."MANDT"[VARCHAR2,9], "A"."NPDAT"[VARCHAR2,24], "A"."NPSEQ"[VARCHAR2,15], "A"."APPRSTEP"[VARCHAR2,6], "A"."APPRDAT"[VARCHAR2,24]
   6 - "A"."MANDT"[VARCHAR2,9], "A"."KUNNR"[VARCHAR2,30], "A"."NPDAT"[VARCHAR2,24], "A"."NPSEQ"[VARCHAR2,15], "A"."APPRSTEP"[VARCHAR2,6],
       "A"."APPRDAT"[VARCHAR2,24]
   7 - "A"."MANDT"[VARCHAR2,9], "A"."KUNNR"[VARCHAR2,30], "A"."NPDAT"[VARCHAR2,24], "A"."NPSEQ"[VARCHAR2,15], "A"."APPRSTEP"[VARCHAR2,6],
       "A"."STATUS"[VARCHAR2,6], "A"."APPRDAT"[VARCHAR2,24]
   8 - "MANDT"[VARCHAR2,9], "KUNNR"[VARCHAR2,30], "NPDAT"[VARCHAR2,24], "NPSEQ"[VARCHAR2,15], "APPRSTEP"[VARCHAR2,6], MAX("STATUS")[6], MAX("APPRDAT")[24]
   9 - "MANDT"[VARCHAR2,9], "KUNNR"[VARCHAR2,30], "NPDAT"[VARCHAR2,24], "NPSEQ"[VARCHAR2,15], "APPRSTEP"[VARCHAR2,6], "APPRID"[VARCHAR2,30],
       MAX("STATUS")[6], MAX("APPRDAT")[24]
  10 - "MANDT"[VARCHAR2,9], "KUNNR"[VARCHAR2,30], "NPDAT"[VARCHAR2,24], "NPSEQ"[VARCHAR2,15], "APPRSTEP"[VARCHAR2,6], "APPRID"[VARCHAR2,30],
       "STATUS"[VARCHAR2,6], "APPRDAT"[VARCHAR2,24]
  11 - "MANDT"[VARCHAR2,9], "KUNNR"[VARCHAR2,30], "NPDAT"[VARCHAR2,24], "NPSEQ"[VARCHAR2,15], "APPRSTEP"[VARCHAR2,6], "APPRID"[VARCHAR2,30],
       "STATUS"[VARCHAR2,6], "APPRDAT"[VARCHAR2,24]
  14 - "A2"."MANDT"[VARCHAR2,9]
  15 - (#keys=4) "MANDT"[VARCHAR2,9], "NPDAT"[VARCHAR2,24], "NPSEQ"[VARCHAR2,15], TO_NUMBER("APPRSTEP")+1[22]
  16 - "MANDT"[VARCHAR2,9], "NPDAT"[VARCHAR2,24], "NPSEQ"[VARCHAR2,15], "APPRSTEP"[VARCHAR2,6]
  17 - "MANDT"[VARCHAR2,9], "NPDAT"[VARCHAR2,24], "NPSEQ"[VARCHAR2,15], "APPRSTEP"[VARCHAR2,6]
  18 - "ZSMTN31".ROWID[ROWID,10], "MANDT"[VARCHAR2,9], "NPDAT"[VARCHAR2,24], "NPSEQ"[VARCHAR2,15], "APPRSTEP"[VARCHAR2,6]
