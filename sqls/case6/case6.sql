--ctdpsv1ps5cj5
var b1 varchar2(32);
var b2 varchar2(32);
var b3 varchar2(32);
var b4 varchar2(32);
var b5 varchar2(32);

exec :b1 := '100'             ;
exec :b2 := '100'             ;
exec :b3 := '100'             ;
exec :b4 := 'S000000756'      ;
exec :b5 := 'M000044273'      ;

SELECT /*+ Index(B "ZSDT201~Z05") */ count(*) as input
FROM sapsr3.ZSDT201 B
,(
  SELECT *
  FROM sapsr3.ZSDT200
  WHERE MANDT = :b1 AND SYSID NOT IN
  (SELECT SYSID
  FROM sapsr3.ZSDT241
  WHERE MANDT = :b2)
 ) A 
,sapsr3.MARA D 
,sapsr3.ZSMTN20 C
WHERE B.MANDT = :b3 
  AND B.NODEKEY = :b4 
  AND B.MEMID = :b5
  AND A.MANDT = B.MANDT
  AND A.ZVBELN = B.ZVBELN
  AND A.PREDLVLIFNR != ' '
  AND B.MANDT = D.MANDT
  AND B.MATNR = D.MATNR
  AND B.MANDT = C.MANDT
  AND B.ZVBELN = C.ZVBELN
  AND C.PREDLVFMD = 'X'
  AND C.PREDLVLIFNR != ' '
  AND C.AUTOVBELNF = ' '
  AND B.STATUS = 'A02'
;