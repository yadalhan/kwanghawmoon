-- for exadata
-- http://uhesse.com/exadata/ 
select name,value/1024/1024 as mb from v$statname natural join v$mystat
where name in
('cell physical IO interconnect bytes returned by smart scan',
'cell physical IO bytes saved by storage index');

alter session set cell_offload_processing=true;
 
select
*
from DBA_HIST_SQLBIND
WHERE sql_id = '2xb05g5vpy0zn'
and snap_id > 71 and instance_number = 2


select * from dba_ind_columns 
where table_name = 'ZSMTN42'
order by index_name, column_position

select * from sapsr3.zhrt227
where addr like '����%'

SELECT
owner, table_name,num_rows, avg_row_len,last_analyzed
--t.*
FROM dba_tab_statistics t
WHERE table_name in (
 'KNA1'
,'T179T'
,'ZSDT001'
,'ZSDT005'
,'ZSMT901'
,'ZSMTN10'
,'ZSMTN20'
,'ZSMTN40'
,'ZSMTN41'
,'ZSMTN42'
,'ZSMTN43'
)
order by table_name

select s.sql_id,s.instance_number, s.snap_id, to_char(h.begin_interval_time,'yy/mm/dd hh24') begin_time
, executions_delta,round(elapsed_time_delta/executions_delta/1000000,2) ela_exe
, round(cpu_time_delta/executions_delta/1000000,2) cpu_exe
, round(buffer_gets_delta/executions_delta,2) gets_exe
, round(clwait_delta/executions_delta,2) clwaits_exe
, round(fetches_delta/executions_delta,2) fetches_exe
, round(rows_processed_delta/executions_delta,2) processed_exe
, round(iowait_delta/executions_delta,2) io_wait_exe
, round(apwait_delta/executions_delta,2) ap_wait_exe
, round(IO_OFFLOAD_RETURN_BYTES_DELTA/executions_delta,2) offload_exe
, round(IO_INTERCONNECT_BYTES_delta/executions_delta,2) interconnect_exe
from (
   select sql_id,instance_number, snap_id
   , decode( executions_delta, 0, 1,executions_delta ) executions_delta
   , elapsed_time_delta, cpu_time_delta, buffer_gets_delta, clwait_delta, fetches_delta, rows_processed_delta
   , IO_INTERCONNECT_BYTES_delta, IO_OFFLOAD_ELIG_BYTES_DELTA,IO_OFFLOAD_RETURN_BYTES_DELTA, IOWAIT_DELTA,APWAIT_DELTA
   from dba_hist_sqlstat
   where sql_id = 'cammmz7ck73k4'
)   s , dba_hist_snapshot h
where s.instance_number = h.instance_number and s.snap_id = h.snap_id
order by s.instance_number desc, s.snap_id;
