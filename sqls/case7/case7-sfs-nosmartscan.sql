--  c7bytm35ya4xw
--'%서울시 금천구 가산동 459-9번지 LG가산디지털단지내%'
--alter session set cell_offload_processing=false;
var a0 varchar2(32 );
var a1 varchar2(2000);
var a2 varchar2(32);
var a3 number;

exec :a0 := '100';
exec :a1 := '%경기 파주시 월롱면 덕은리 파주LCD산업단지LCD TV공장신축현장%';
exec :a2 := 'X';
exec :a3 := 1;
SELECT /*+ full(ZHRT227) opt_param('cell_offload_processing','false')*/ "ZIPCD"
FROM sapsr3.ZHRT227
WHERE "MANDT"=:A0
AND "ADDR" LIKE :A1
AND "STATUS"=:A2
AND ROWNUM <=:A3
;
--alter session set cell_offload_processing=true;