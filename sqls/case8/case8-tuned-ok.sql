-- 7wmkba6r1g3n9
var b1 varchar2(32)
var b2 varchar2(32)
var b3 varchar2(32)
var b4 varchar2(32)
var b5 varchar2(32)
var b6 varchar2(32)
var b7 varchar2(32)

exec :b1 := '100'        ;     
exec :b2 := '100'        ;
exec :b3 := '0000024315' ;
exec :b4 := '100'        ;
exec :b5 := '0000024315' ;
exec :b6 := '0000024315' ;
exec :b7 := '0001'       ;

SELECT /*+ LEADING(ND) USE_NL(ND NH NB) INDEX(ND "ZSMTN41~Z02") INDEX(NH "ZSMTN10~0") INDEX(NB "ZSMTN40~Z02")*/ ND.BIDSEQ,
       ND.BIDITEMNO,
       ' ' RECDAT, NP.STATUS,
  (SELECT CD_NM
   FROM sapsr3.ZSMT901
   WHERE MANDT=:b1
     AND CD_CLF='ZSTDF'
     AND CD =ND.STDF) STDFNM, NB.BIDMDID OPID, NP.NPTEMPNR,
  (SELECT VTEXT
   FROM sapsr3.T179T
   WHERE MANDT = ND.MANDT
     AND PRODH = ND.PRODH
     AND SPRAS = '3') PRODHNM, ND.PRODNMKOR, TRIM(KN.NAME3||KN.NAME4) KNANAME, CASE
                                                                                   WHEN NP.STATUS IN('42') THEN '�ڵ�����������'
                                                                                   WHEN NP.STATUS IN('40','42','60') THEN '������(������)'
                                                                                   WHEN NP.STATUS IN('40','42') THEN '������'
                                                                                   WHEN NP.STATUS = '60' THEN '������'
                                                                                   WHEN NP.STATUS IN('45','50','51','52','53', '55', '58', '62') THEN '���������'
                                                                                   WHEN NP.STATUS IN('70','80') THEN '��������'
                                                                               END STATUSNM,
  (SELECT MEMBERNM
   FROM sapsr3.ZSDT001
   WHERE MANDT = NH.MANDT
     AND MEMID = NH.MEMID) MEMNM,
  (SELECT city1
   FROM sapsr3.zsdt001
   WHERE mandt=nh.mandt
     AND memid = nh.memid) addr, TO_CHAR(TO_DATE(ND.NPDAT,'YYYY.MM.DD'),'YYYY.MM.DD') SDATE, CASE
                                                                                                 WHEN NP.STATUS IN('42') THEN TO_CHAR(TO_DATE(NB.BIDAUTOENDDAT||' '||NB.BIDAUTOENDZET,'YYYY.MM.DD HH24:MI:SS'),'YYYY.MM.DD HH24:MI:SS')
                                                                                                 ELSE TO_CHAR(TO_DATE(NB.BIDENDDAT||' '||NB.BIDENDZET,'YYYY.MM.DD HH24:MI:SS'),'YYYY.MM.DD HH24:MI:SS')
                                                                                             END EDATE,
  (SELECT COUNT(*)
   FROM sapsr3.ZSMTN43
   WHERE MANDT=:b2
     AND BIDSEQ = ND.BIDSEQ
     AND BIDITEMNO = ND.BIDITEMNO
     AND LIFNR = :b3
     AND STATUS <> '40'
     AND APPLYPRICE > 0) BIDCNT,
   OP.ENAME,
   DECODE(ND.CONTRACTF,'X','���ǰ','�Ϲ�') CONTRACTFNM
FROM sapsr3.ZSMTN41 ND
INNER JOIN sapsr3.ZSMTN40 NB 
  ON NB.MANDT = ND.MANDT
  AND NB.BIDSEQ = ND.BIDSEQ
INNER JOIN sapsr3.ZSMTN10 NH 
  ON NH.MANDT = ND.MANDT
  AND NH.KUNNR = ND.KUNNR
  AND NH.NPDAT = ND.NPDAT
  AND NH.NPSEQ = ND.NPSEQ
INNER JOIN sapsr3.ZSMTN20 NP 
  ON NP.MANDT = ND.MANDT
  AND NP.KUNNR = ND.KUNNR
  AND NP.NPDAT = ND.NPDAT
  AND NP.NPSEQ = ND.NPSEQ
  AND NP.NPITEMNO = ND.NPITEMNO
INNER JOIN sapsr3.ZSMT901 CT 
  ON CT.MANDT = ND.MANDT
  AND CT.CD_CLF = 'ZSTATUS12'
  AND CT.CD = NP.STATUS
INNER JOIN sapsr3.ZSDT001 ME 
  ON ME.MANDT = ND.MANDT
  AND ME.MEMID = NH.MEMID
LEFT OUTER JOIN sapsr3.ZSDT005 OP 
  ON OP.MANDT = ND.MANDT
  AND OP.OPID = NB.BIDMDID
INNER JOIN sapsr3.KNA1 KN 
  ON KN.MANDT = ND.MANDT
  AND KN.KUNNR = ND.KUNNR
WHERE ND.MANDT = :b4
  AND ND.STATUS NOT IN ('30',
                        '90')
  AND NB.BIDPUBF = ' '
  AND NOT EXISTS
    (SELECT 'X'
     FROM sapsr3.ZSMTN42
     WHERE MANDT = ND.MANDT
       AND BIDSEQ = ND.BIDSEQ
       AND BIDITEMNO = ND.BIDITEMNO
       AND LIFNR = :b5)
  AND NOT EXISTS
    (SELECT 'X'
     FROM sapsr3.ZSMTN43
     WHERE MANDT = ND.MANDT
       AND BIDSEQ = ND.BIDSEQ
       AND BIDITEMNO = ND.BIDITEMNO
       AND LIFNR = :b6
       AND STATUS = '50')
  AND NP.STATUS IN ('40',
                    '42',
                    '60')
  AND ND.PRODH LIKE :b7||'%';